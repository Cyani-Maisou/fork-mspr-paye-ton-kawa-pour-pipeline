FROM php:8.2-apache  

COPY webservice /var/www/html/webservice

COPY conf/payetonkawa_api.conf /etc/apache2/sites-available/payetonkawa_api.conf

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf 
RUN a2enmod rewrite
RUN a2enmod headers && sed -ri -e 's/^([ \t]*)(<\/VirtualHost>)/\1\tHeader set Access-Control-Allow-Origin "*"\n\1\2/g' /etc/apache2/sites-available/*.conf

RUN a2dissite 000-default
RUN a2ensite payetonkawa_api
RUN service apache2 restart

RUN apt-get -y update
RUN apt-get -y install git

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer --version

RUN cd /var/www/html/webservice && composer install

EXPOSE 80

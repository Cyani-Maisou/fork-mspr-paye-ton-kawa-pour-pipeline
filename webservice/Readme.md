# dépendances

faire un `composer install` dans `/webservice`

# pour send mail

https://kenfavors.com/code/how-to-install-and-configure-sendmail-on-ubuntu/

# config générale apache

- dans `/etc/apache2/sites-available/000-default.conf`

  ```
  <Directory "/var/www/html">
  AllowOverride All
  </Directory>
  ```

- dans `/etc/apache2/apache2.conf`

  ```
  <ifModule mod_rewrite.c>
  RewriteEngine On
  </ifModule>
  ```

- ajouter le module rewrite : `a2enmod rewrite`

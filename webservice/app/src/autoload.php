<?php
/**
 * un equivalent de str_starts_with pour ceux qui ont pas php8
 */
if (!function_exists('str_starts_with'))
{
  function str_starts_with($str, $start) {
    return (@substr_compare($str, $start, 0, strlen($start))==0);
  }
}

/**
 * scanne une arborescence
 */
function getChildDirs($dir)
{
  $dirs_list = [];
  $dir_content = scandir($dir);

  foreach($dir_content as $key => $item){
    if(is_dir(realpath($dir."/".$item)) && $item != '.' && $item != '..' && !str_starts_with($item, '.')){
      $dirs_list[] = $dir.'/'.$item;
      $res = getChildDirs($dir.'/'.$item);

      foreach($res as $key => $value){
        $dirs_list[] = $value;
      }
    }
  }
  return $dirs_list;
}

/**
 * recherche un classe dans l'arborescence
 */
spl_autoload_register(function ($class_name)
{
  $dirs = getChildDirs('.');

  foreach($dirs as $dir){
    $file = $dir."/".$class_name.'.php';
    if(file_exists($file)){
      include $file;
      break;
    }
  }
});
?>
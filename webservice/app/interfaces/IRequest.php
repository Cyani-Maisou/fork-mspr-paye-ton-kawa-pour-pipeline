<?php

interface IRequest
{
  public function uri(): string;
  public function method(): string;
  public function body(): array;
  public function headers(): array;
}
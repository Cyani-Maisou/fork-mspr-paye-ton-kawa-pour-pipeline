<?php

interface IRouter
{
  public function initRoute(string $method, string $nom, string $controllerAction, string $middlewareName = ''): void;
  public function getControllerAction(IRequest $request): Route;
  public function getVarsFromUri(IRequest $request, Route $route): array;
}
<?php

interface IMiddleware 
{
  public function middlewareAction(IRequest $request): void;
}
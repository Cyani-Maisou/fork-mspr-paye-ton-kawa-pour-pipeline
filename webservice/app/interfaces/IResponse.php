<?php

interface IResponse
{
  public function toJson($code, $payload = null): void;
}
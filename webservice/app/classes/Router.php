<?php

class Router implements IRouter
{
  private array $_routes;

  public function __construct() 
  {
    $this->_routes = array();
  }

  public function initRoute(string $method, string $nom, string $controllerAction, string $middlewareName = ''): void
  {
    $this->_routes[] = new Route($nom, $method, $nom, $controllerAction, $middlewareName);
  }

  public function getControllerAction(IRequest $request): Route
  {
    foreach($this->_routes as $key => $route){
      if(preg_match('#^'.$route->pattern().'$#', $request->uri()) && $route->method() === $request->method()){
        return $route;

        break;
      }
    }

    throw new Exception('Not Found', 404);
  }

  public function getVarsFromUri(IRequest $request, Route $route): array
  {
    $params = array();
    $search = '/'.preg_quote($route->pattern(), '/').'/';
    preg_match('#^'.$route->pattern().'$#', $request->uri(), $params);

    if (count($params) > 1) {
      unset($params[0]);
      return $this->sanitizeParams($params, $route);
    } else {
      return array();
    }
  }

  private function sanitizeParams(array $params, Route $route): array
  {
    $cleanIndexesParams = array();

    foreach($params as $key => $value) {
      $cleanIndexesParams[] = $value;
    }

    preg_match_all('/string|int/', $route->name(), $types);
    $types = $types[0];

    if(count($cleanIndexesParams) == count($types)) {
      for ($i = 0; $i < count($cleanIndexesParams); $i++) {
        settype($cleanIndexesParams[$i], $types[$i]);
      }
    }

    return $cleanIndexesParams;
  }

  public function routes(): array
  {
    return $this->_routes;
  }
}
<?php

class Request implements IRequest
{
  private string $_uri;
  private string $_method;
  private array $_body = array();
  private array $_headers = array();

  public function __construct()
  {
    $this->_uri = $_SERVER['REQUEST_URI'];

    if($this->_uri[0] === '/') {
      $count = 1;
      $this->_uri = preg_replace('#'.getenv('BASE_ROUTE').'#', '', $this->_uri, $count);
    }

    if(substr($this->_uri, -1) != '/') {
      $this->_uri .= '/';
    }

    $this->_method = $_SERVER['REQUEST_METHOD'];
    $rawBody = file_get_contents('php://input');
    $body = json_decode($rawBody, true);

    if (!empty($body)) {
      $this->_body = $body;
    }

    $this->_headers = array_change_key_case(getallheaders(), CASE_LOWER);
  }

  public function uri(): string
  {
    return $this->_uri;
  }

  public function method(): string
  {
    return $this->_method;
  }

  public function body(): array
  {
    return $this->_body;
  }
  
  public function headers(): array
  {
    return $this->_headers;
  }
}
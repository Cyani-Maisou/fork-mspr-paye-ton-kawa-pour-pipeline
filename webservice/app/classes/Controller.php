<?php

abstract class controller
{
  private static IRequest $_request;
  private static IResponse $_response;

  public static function setRequest(IRequest $req): void
  {
    self::$_request = $req;
  }

  protected static function request(): IRequest
  {
    return self::$_request;
  }

  public static function setResponse(IResponse $resp): void
  {
    self::$_response = $resp;
  }

  protected static function response(): IResponse
  {
    return self::$_response;
  }
}
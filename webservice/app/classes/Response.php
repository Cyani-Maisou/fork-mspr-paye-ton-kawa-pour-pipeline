<?php

class Response implements IResponse
{
  private $_code;
  private $_message;
  private $_payload;

  public function toJson($code, $payload = null): void
  {
      $this->_code = $code;
      $this->_payload = $payload;
      $this->getMessage();
      $this->render();
  }

  private function render(): void
  {
      header("HTTP/1.1 ".$this->_message);

      $rep = null;

      if ($this->_payload) {
          $rep = $this->_payload;
      }

      header('Content-Type: application/json; charset=utf-8');
      echo json_encode($rep, JSON_PRETTY_PRINT);
  }

  private function getMessage(): void
  {
    switch($this->_code) {
      case 200:
        $this->_message = '200 Ok';
        break;
    case 201:
        $this->_message = '201 Created';
        break;
    case 204:
        $this->_message = '204 No Content';
        break;
    case 400:
        $this->_message = '400 Bad Request';
        break;
    case 401:
        $this->_message = '401 Unauthorized';
        break;
    case 403:
        $this->_message = '403 Forbidden';
        break;
    case 404:
        $this->_message = '404 Not Found';
        break;
    case 405:
        $this->_message = '405 Method Not Allowed';
        break;
    case 498:
        $this->_message = '498 Token expired/invalid';
        break;
    case 500:
        $this->_message = '500 Internal Server Error';
        break;
    case 501:
        $this->_message = '501 Not Implemented';
        break;
    case 503:
        $this->_message = '503 Service Unavailable';
        break;
    }
  }
}

<?php

abstract class Dispatcher
{
  private static IRouter $_router;
  private static IRequest $_request;
  private static IResponse $_response;

  public static function resolveRoute(IRequest $req, IResponse $response): void
  {
    self::$_request = $req;
    self::$_response = $response;

    Controller::setRequest($req);
    Controller::setResponse($response);

    try {
      $route = self::$_router->getControllerAction($req);
      $params = self::$_router->getVarsFromUri($req, $route);

      // vérification et appel du middleware
      if ($route->middlewareName()) {
        $middleware = $route->middlewareName();

        $methodeMiddleware = array(new $middleware, "middlewareAction");
        $middlewareParams = array($req);

        self::callMethod($methodeMiddleware, $middlewareParams);
      }

      // récupération et appel du controller
      $controller = $route->controllerName();
      $methode = array(new $controller, $route->actionName());

      self::callMethod($methode, $params);
    } catch (ICustomException $e) {
      $handler = new CustomExceptionHandler($response);
      $handler->handle($e);
    } catch (Exception $e) {
      $infos['message'] = $req->uri().' : endpoint not found';
      self::$_response->toJson(404, $infos);
    }
  }

  private static function callMethod(array $methode, array $params = null): void
  {
    if(is_callable($methode, false, $callable_name)) {
      // si il y avait un paramètre
      if(!empty($params)) {
        call_user_func_array($methode, $params);
      } else {
        call_user_func($methode);
      }
    } else {
      $infos['error'] = $methode[0].'.'.$methode[1].' is not callable ('.$callable_name.')';
      self::$_response->toJson(500, $infos);
    }
  } 

  public static function setRouter(IRouter $router): void
  {
    self::$_router = $router;
  }

  public static function request(): IRequest
  {
    return self::$_request;
  }
}
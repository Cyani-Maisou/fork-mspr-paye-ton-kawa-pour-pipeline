<?php

abstract class DotEnv
{
  public static function load($path): void 
  {
    if(!file_exists($path)) {
      throw new \InvalidArgumentException(sprintf('%s n\'exite pas', $path));
    }

    if(!is_readable($path)) {
      throw new \RuntimeException(sprintf('%s n\'est pas lisible', $path));
    }

    $lines = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

    foreach($lines as $line) {
      // on regarde si la ligne est commentée
      if(strpos(trim($line), '#') === 0) {
          continue;
      }
      
      $env_var = explode('=', $line, 2);

      $var_name = trim($env_var[0]);
      $var_value = trim($env_var[1]);

      // si la var n'existe pas déjà
      if(!array_key_exists($var_name, $_SERVER) && !array_key_exists($var_name, $_ENV)) {
        putenv($var_name."=".$var_value);
        $_ENV[$var_name] = $var_value;
        $_SERVER[$var_name] = $var_value;
      }
    }
  }
}
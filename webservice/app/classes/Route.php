<?php

class Route
{
  private string $_name;
  private string $_method;
  private string $_pattern;
  private string $_controllerName;
  private string $_actionName;
  private string $_middlewareName;

  public function __construct(
    string $name, 
    string $method,
    string $pattern,
    string $controllerAction,
    string $middlewareName = ''
  ) {

    if(substr($name, -1) != '/') {
      $name .= '/';
    }
    if(substr($pattern, -1) != '/') {
      $pattern .= '/';
    }

    $pattern = str_replace('/', '\/', $pattern);
    $pattern = str_replace('{int}', '([0-9]+)', $pattern);
    $pattern = str_replace('{string}', '([a-zA-ZÀ-ÿ]+)', $pattern);

    $controllerAction = explode('.', $controllerAction);

    $this->_name = $name;
    $this->_method = $method;
    $this->_pattern = $pattern;
    $this->_controllerName = ucfirst($controllerAction[0]);
    $this->_actionName = $controllerAction[1];
    $this->_middlewareName = ucfirst($middlewareName);
  }

  public function name(): string
  {
    return $this->_name;
  }

  public function method(): string
  {
    return $this->_method;
  }

  public function pattern(): string
  {
    return $this->_pattern;
  }

  public function controllerName(): string
  {
    return $this->_controllerName;
  }

  public function actionName(): string
  {
    return $this->_actionName;
  }

  public function middlewareName(): string
  {
    return $this->_middlewareName;
  }
}
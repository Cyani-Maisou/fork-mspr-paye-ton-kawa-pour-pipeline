<?php

$router->initRoute('POST', '/revendeurs/login', 'AuthController.login');
$router->initRoute('POST', '/revendeurs/register', 'AuthController.register');
$router->initRoute('POST', '/revendeurs/validate', 'AuthController.validate');

// --- API Revendeurs ---

// Routes Products
$router->initRoute('GET', '/revendeurs/products', 'ProductController.getAllProducts', 'AuthMiddleware');
$router->initRoute('GET', '/revendeurs/product/{int}', 'ProductController.getOneProductById', 'AuthMiddleware');

// ----------------------

// ---- API Webshop -----

// Routes Customers
$router->initRoute('GET', '/webshop/customers', 'CustomerController.getAllCustomers', 'WebshopAuthMiddleware');
$router->initRoute('GET', '/webshop/customer/{int}', 'CustomerController.getOneCustomerById', 'WebshopAuthMiddleware');

// Routes Orders
$router->initRoute('GET', '/webshop/customer/{int}/orders', 'OrderController.getAllOrdersOfACustomer', 'WebshopAuthMiddleware');
$router->initRoute('GET', '/webshop/customer/{int}/order/{int}', 'OrderController.getOneOrderOfACustomerById', 'WebshopAuthMiddleware');

// Routes Products
$router->initRoute('GET', '/webshop/products', 'ProductController.getAllProducts', 'WebshopAuthMiddleware');
$router->initRoute('GET', '/webshop/product/{int}', 'ProductController.getOneProductById', 'WebshopAuthMiddleware');
$router->initRoute('GET', '/webshop/customer/{int}/order/{int}/products', 'ProductController.getProductsOfAnOrder', 'WebshopAuthMiddleware');

// ----------------------
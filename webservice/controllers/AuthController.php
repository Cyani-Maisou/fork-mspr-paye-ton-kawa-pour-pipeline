<?php

class AuthController extends Controller
{
  public function login(): void
  {
    $body = self::request()->body();

    if (empty($body["username"]) || empty($body["password"])) {
      $rep["message"] = "Error: please provide a unsername and a password";
      
      self::response()->toJson(400, $rep);
    } else {
      $user = UserDAL::getByUserName($body["username"]);

      if (
        !empty($user) && 
        password_verify($body["password"], $user->password()) &&
        $user->validationKey() === null
      ) {
        $jwt = JWTHelper::generate($body);

        $rep["access_token"] = $jwt;

        self::response()->toJson(200, $rep);
      } else {
        throw new InvalidAuthException("Error: invalid unsername or password", 400);
      }
    }
  }

  public function register(): void
  {
    $body = self::request()->body();

    if (!empty($body["username"]) && !empty($body["password"]) && !empty($body["mail"])) {
      // TODO: vérifier si l'user existe pas déjà !!!
      if ($user = UserDAL::getByUserName($body["username"])) {
        throw new InvalidAuthException("Error: user already exists.", 400);
      }

      $validationKey = $this->generateRandomString();

      UserDAL::addUser($body["username"], $body["password"], $body["mail"], $validationKey);

      $rep["message"] = "user added.";

      $qrData = array(
        "validationKey" => $validationKey
      );

      $qrCode = QRHelper::generateQR($qrData);

      $result = MailHelper::sendValidationKey($body['mail'], $qrCode);

      $rep["mailSent"] = $result;

      self::response()->toJson(201, $rep);
    } else {
      $rep["message"] = "please provide a username, a password and an email adress.";
      self::response()->toJson(400, $rep);
    }
  }

  public function validate(): void
  {
    $body = self::request()->body();
    $user = UserDAL::getByUserName($body["username"]);

    if (empty($user)) {
      throw new InvalidAuthException("Error: invalid unsername", 400);
    }
    
    if($user->validationKey() === $body["validationKey"]) {
      $user->setValidationKey(null);
      UserDAL::updateUser($user);
    } else {
      $rep["message"] = "Error: invalid validation key.";
      self::response()->toJson(400, $rep);
    }
  }

  private function generateRandomString($length = 25) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
  }
}


<?php

use GuzzleHttp\Exception\ClientException;

abstract class CommonController extends Controller
{
    protected function handleGuzzleException($e): void
    {
        if ($e->getCode() == 400) {
        throw new ApiException("Error: customer not found", 404);
        } else {
        self::response()->toJson($e->getCode(), $e->getMessage());
        }
    }
}

?>
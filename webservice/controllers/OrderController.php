<?php

class OrderController extends CommonController
{
  public function getAllOrdersOfACustomer(int $idCustomer): void 
  {
    try{
      $orders = OrderDAL::getAllOrdersOfACustomer($idCustomer);

      $rep = array();
      foreach($orders as $order){
        $rep[] = $order->toArray();
      }

      self::response()->toJson(200, $rep);
    } catch (GuzzleHttp\Exception\ClientException $e) {
      $this->handleGuzzleException($e);
    }
  }

  public function getOneOrderOfACustomerById(int $idCustomer, int $idOrder): void 
  {
    try {
      $order = OrderDAL::getOneOrderOfACustomerById($idCustomer, $idOrder);

      $rep = $order->toArray();

      self::response()->toJson(200, $rep);
    } catch (GuzzleHttp\Exception\ClientException $e) {
      $this->handleGuzzleException($e);
    }
  }
}

?>
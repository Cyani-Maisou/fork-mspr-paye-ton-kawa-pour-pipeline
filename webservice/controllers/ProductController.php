<?php

class ProductController extends CommonController
{
  public function getAllProducts(): void 
  {
    try{
      $products = ProductDAL::getAllProducts();

      $rep = array();
      foreach($products as $product){
        $rep[] = $product->toArray();
      }

      self::response()->toJson(200, $rep);
    } catch (GuzzleHttp\Exception\ClientException $e) {
      $this->handleGuzzleException($e);
    }
  }

  public function getOneProductById(int $id): void 
  {
    try {
      $product = ProductDAL::getOneProductById($id);

      $rep = $product->toArray();

      self::response()->toJson(200, $rep);
    } catch (GuzzleHttp\Exception\ClientException $e) {
      $this->handleGuzzleException($e);
    }
  }

  public function getProductsOfAnOrder(int $idCustomer, int $idOrder): void 
  {
    try {
      $products = ProductDAL::getProductsOfAnOrder($idCustomer, $idOrder);

      $rep = array();
      foreach($products as $product){
        $rep[] = $product->toArray();
      }

      self::response()->toJson(200, $rep);
    } catch (GuzzleHttp\Exception\ClientException $e) {
      $this->handleGuzzleException($e);
    }
  }
}

?>
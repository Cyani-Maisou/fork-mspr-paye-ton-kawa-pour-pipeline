<?php

class CustomerController extends CommonController
{
  public function getAllCustomers(): void 
  {
    try{
      $customers = CustomerDAL::getAllCustomers();

      $rep = array();
      foreach($customers as $customer){
        $rep[] = $customer->toArray();
      }

      self::response()->toJson(200, $rep);
    } catch (GuzzleHttp\Exception\ClientException $e) {
      $this->handleGuzzleException($e);
    }
  }

  public function getOneCustomerById(int $id): void 
  {
    try {
      $customer = CustomerDAL::getOneCustomerById($id);

      $rep = $customer->toArray();

      self::response()->toJson(200, $rep);
    } catch (GuzzleHttp\Exception\ClientException $e) {
      $this->handleGuzzleException($e);
    }
  }
}

?>
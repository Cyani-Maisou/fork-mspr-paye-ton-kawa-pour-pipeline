<?php

class CustomExceptionHandler
{
  private IResponse $_response;

  public function __construct(IResponse $rep) 
  {
    $this->_response = $rep;
  }

  public function handle(ICustomException $e): void
  {
    switch (get_class($e)) 
    {
      case 'InvalidAuthException':
        $infos['message'] = $e->getMessage();
        $this->_response->toJson($e->getCode(), $infos);
        break;
      case 'ApiException':
        $infos['message'] = $e->getMessage();
        $this->_response->toJson($e->getCode(), $infos);
    }
  } 
}
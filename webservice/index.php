<?php

require(__DIR__.'/app/src/autoload.php');
require 'vendor/autoload.php';
DotEnv::load(__DIR__.'/.env');

$request = new Request();
$router = new Router();
$response = new Response();
Dispatcher::setRouter($router);

require('app/routes.php');

Dispatcher::resolveRoute($request, $response);

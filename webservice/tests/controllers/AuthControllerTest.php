<?php
declare(strict_types=1);
use PHPUnit\Framework\TestCase;

/**
 * @covers AuthController
 */
final class AuthControllerTest extends TestCase
{
  public function testLoginReturnAJwt() : void
  {
    $request = new MockedLoginRequest();
    $controller = new AuthController();
    $response = new MockedResponse();
    Controller::setRequest($request);
    Controller::setResponse($response);
    $jwtPattern = '/([A-Za-z1-9_])+\.([A-Za-z1-9_])+\.([A-Za-z1-9_])+/';

    $controller->login();
    $rep = MockedResponse::$calledPayload;

    preg_match($jwtPattern, $rep["access_token"], $matches);

    $this->assertTrue(!empty($matches));
  }
  public function testInvalidLoginThrowsAnException() : void
  {
    $this->expectException(InvalidAuthException::class);

    $request = new MockedInvalidLoginRequest();
    $controller = new AuthController();
    $response = new MockedResponse();
    Controller::setRequest($request);
    Controller::setResponse($response);

    $controller->login();
    $rep = MockedResponse::$calledPayload;
  }
}

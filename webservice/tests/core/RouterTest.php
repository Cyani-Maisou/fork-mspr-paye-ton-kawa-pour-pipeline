<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

/**
 * @covers Router
 */
final class RouterTest extends TestCase
{
  public function testInitRouteMethodAddARouteToRouter(): void
  {
    $router = new Router();
    $expectedCount = 1;

    $router->initRoute('POST', '/revendeurs/login', 'AuthController.login');

    $nbRoutes = count($router->routes());
    $this->assertSame($expectedCount, $nbRoutes);
  }

  public function testGetControllerActionReturnsARoute(): void
  {
    $router = new Router();
    $router->initRoute('POST', '/revendeurs/login', 'AuthController.login');
    $request = new MockedRequestPostWithoutParam();

    $res = $router->getControllerAction($request);

    $this->assertInstanceOf(
      Route::class,
      $res
    );
  }

  public function testIfNoRouteIfFoundAnExceptionIsThrown(): void
  {
    $this->expectException(Exception::class);

    $router = new Router();
    $request = new MockedRequestPostWithoutParam();

    $res = $router->getControllerAction($request);
  }

  public function testRightVariablesAnTypesAreReturned(): void
  {
    $router = new Router();
    $router->initRoute('GET', '/test/{string}/{int}', 'AuthController.login');
    $request = new MockedRequestGetWithParams();
    $expected = array(
      'coucou',
      1
    );

    $route = $router->getControllerAction($request);
    $actual = $router->getVarsFromUri($request, $route);

    $this->assertEquals($expected, $actual);
  }
}

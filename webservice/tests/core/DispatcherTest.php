<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

/**
 * @covers Dispatcher
 */
final class DispatcherTest extends TestCase
{
  public function testResolveRouteCallTheRightControllerMethod(): void
  {
    $router = new MockedRouterWithoutMiddleware();
    $request = new MockedRequestGetWithParams();
    $response = new MockedResponse();
    MockedController::$methodHasBeenCalled = false;

    Dispatcher::setRouter($router);

    Dispatcher::resolveRoute($request, $response);

    $this->assertTrue(MockedController::$methodHasBeenCalled);
  }

  public function testIfThereIsAMiddlewareItIsCalled(): void
  {
    $router = new MockedRouterWithMiddleware();
    $request = new MockedRequestGetWithParams();
    $response = new MockedResponse();
    MockedMiddleware::$methodHasBeenCalled = false;

    Dispatcher::setRouter($router);

    Dispatcher::resolveRoute($request, $response);

    $this->assertTrue(MockedMiddleware::$methodHasBeenCalled);
  }

  public function testIfThereIsAMiddlewareTheRightControllerIsCalled(): void
  {
    $router = new MockedRouterWithMiddleware();
    $request = new MockedRequestGetWithParams();
    $response = new MockedResponse();
    MockedController::$methodHasBeenCalled = false;

    Dispatcher::setRouter($router);

    Dispatcher::resolveRoute($request, $response);

    $this->assertTrue(MockedController::$methodHasBeenCalled);
  }

  public function testIfNotRouteIsFoundForRequestA404ErrorIsReturnToClient(): void
  {
    $router = new MockedRouterNotFound();
    $request = new MockedRequestPostWithoutParam();
    $response = new MockedResponse();
    MockedResponse::reset();
    $expectedMessage['message'] = '/revendeurs/login/ : endpoint not found';

    Dispatcher::setRouter($router);

    Dispatcher::resolveRoute($request, $response);

    $this->assertTrue(MockedResponse::$hasBeenCalled);
    $this->assertEquals(404, MockedResponse::$calledCode);
    $this->assertEquals($expectedMessage, MockedResponse::$calledPayload);
  }
}


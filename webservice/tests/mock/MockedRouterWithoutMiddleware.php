<?php

class MockedRouterWithoutMiddleware implements IRouter
{
  public function initRoute(string $method, string $nom, string $controllerAction, string $middlewareName = ''): void
  {

  }

  public function getControllerAction(IRequest $request): Route
  {
    return new Route('/test/{string}/{int}', 'GET', '/test/{string}/{int}', 'MockedController.test', '');
  }

  public function getVarsFromUri(IRequest $request, Route $route): array
  {
    return array();
  }
}
<?php

class MockedRouterNotFound implements IRouter
{
  public function initRoute(string $method, string $nom, string $controllerAction, string $middlewareName = ''): void
  {

  }

  public function getControllerAction(IRequest $request): Route
  {
    throw new Exception('Not Found', 404);
  }

  public function getVarsFromUri(IRequest $request, Route $route): array
  {
    return array();
  }
}
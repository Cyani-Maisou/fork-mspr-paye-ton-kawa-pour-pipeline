<?php

class MockedMiddleware implements IMiddleware
{
  public static bool $methodHasBeenCalled = false;
  
  public function middlewareAction(IRequest $request): void
  {
    self::$methodHasBeenCalled = true;
  }
}
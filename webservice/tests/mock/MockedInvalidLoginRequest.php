<?php

class MockedInvalidLoginRequest implements IRequest
{
  public function uri(): string
  {
    return '';
  }
  public function method(): string
  {
    return 'POST';
  }
  public function body(): array
  {
    return [
      "username" => "test",
      "password" => "nope"
    ];
  }
  public function headers(): array
  {
    return [];
  }
}
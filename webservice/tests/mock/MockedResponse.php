<?php

class MockedResponse implements IResponse
{
  public static $calledCode = 0;
  public static $calledPayload = null;
  public static bool $hasBeenCalled = false;

  public function toJson($code, $payload = null): void
  {
    self::$calledCode = $code;
    self::$calledPayload = $payload;
    self::$hasBeenCalled = true;
  }

  public static function reset(): void
  {
    self::$calledCode = 0;
    self::$calledPayload = null;
    self::$hasBeenCalled = false;
  }
}
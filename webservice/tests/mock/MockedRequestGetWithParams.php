<?php

class MockedRequestGetWithParams implements IRequest
{
  public function uri(): string
  {
    return '/test/coucou/1/';
  }
  public function method(): string
  {
    return 'GET';
  }
  public function body(): array
  {
    return [];
  }
  public function headers(): array
  {
    return [];
  }
}
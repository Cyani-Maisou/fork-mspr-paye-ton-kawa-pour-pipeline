<?php

class MockedRequestPostWithoutParam implements IRequest
{
  public function uri(): string
  {
    return '/revendeurs/login/';
  }
  public function method(): string
  {
    return 'POST';
  }
  public function body(): array
  {
    return [];
  }
  public function headers(): array
  {
    return [];
  }
}
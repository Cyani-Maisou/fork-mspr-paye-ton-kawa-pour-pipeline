<?php

class MockedController extends Controller
{
  public static bool $methodHasBeenCalled = false;
  
  public function test(): void
  {
    self::$methodHasBeenCalled = true;
  }
}
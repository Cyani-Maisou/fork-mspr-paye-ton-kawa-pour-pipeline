<?php

abstract class CustomerDAL {

    public static function getAllCustomers() : array
    {
        $url = getenv('API_ROUTE')."/customers";

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        if ($res->getStatusCode() == 200) {
            $body = json_decode($res->getBody());
            return CustomerDAL::createCustomersFromJson($body);
        } else {
            throw new ApiException("Error: Customer not found", 400);
        }
    }

    public static function getOneCustomerById(int $id): Customer
    {
        $url = getenv('API_ROUTE')."/customers/$id";

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        if ($res->getStatusCode() == 200) {
            $body[] = json_decode($res->getBody());
            return CustomerDAL::createCustomersFromJson($body)[0];
        } else {
            throw new ApiException("Error: Customer not found", 400);
        }
    }

    private static function createCustomersFromJson(array $data) : array
    {
        $customers = array();
        foreach ($data as $jsonCustomer) {
            $customer = new Customer(
                $jsonCustomer->id,
                $jsonCustomer->name,
                $jsonCustomer->firstName,
                $jsonCustomer->lastName,
                $jsonCustomer->username,
                $jsonCustomer->profile->firstName,
                $jsonCustomer->profile->lastName,
                $jsonCustomer->address->postalCode,
                $jsonCustomer->address->city,
                $jsonCustomer->company->companyName,
                $jsonCustomer->createdAt
            );
            $customers[] = $customer;
        }
        return $customers;
    }
}

?>
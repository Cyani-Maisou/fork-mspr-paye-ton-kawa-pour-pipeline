<?php

abstract class ProductDAL {

    public static function getAllProducts() : array
    {
        $url = getenv('API_ROUTE')."/products";

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        if ($res->getStatusCode() == 200) {
            $body = json_decode($res->getBody());
            return ProductDAL::createProductsFromJson($body);
        } else {
            throw new ApiException("Error: product not found", 400);
        }
    }

    public static function getOneProductById(int $id): Product
    {
        $url = getenv('API_ROUTE')."/products/$id";

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        if ($res->getStatusCode() == 200) {
            $body[] = json_decode($res->getBody());
            return ProductDAL::createProductsFromJson($body)[0];
        } else {
            throw new ApiException("Error: product not found", 400);
        }
    }

    public static function getProductsOfAnOrder(int $idCustomer, int $idOrder): array
    {
        $url = getenv('API_ROUTE')."/customers/$idCustomer/orders/$idOrder/products";

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        if ($res->getStatusCode() == 200) {
            $body = json_decode($res->getBody());
            return ProductDAL::createProductsFromJson($body);
        } else {
            throw new ApiException("Error: product not found", 400);
        }
    }

    private static function createProductsFromJson(array $data) : array
    {
        $products = array();
        foreach ($data as $jsonProduct) {
            $product = new Product(
                $jsonProduct->id,
                $jsonProduct->name,
                $jsonProduct->stock,
                $jsonProduct->details->price,
                $jsonProduct->details->description,
                $jsonProduct->details->color,
                $jsonProduct->createdAt
            );
            $products[] = $product;
        }
        return $products;
    }
}

?>
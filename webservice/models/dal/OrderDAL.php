<?php

abstract class OrderDAL {

    public static function getAllOrdersOfACustomer(int $idCustomer) : array
    {
        $url = getenv('API_ROUTE')."/customers/$idCustomer/orders";

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        if ($res->getStatusCode() == 200) {
            $body = json_decode($res->getBody());
            return OrderDAL::createOrdersFromJson($body);
        } else {
            throw new ApiException("Error: Order not found", 400);
        }
    }

    public static function getOneOrderOfACustomerById(int $idCustomer, int $idOrder): Order
    {
        $url = getenv('API_ROUTE')."/customers/$idCustomer/orders/$idOrder";

        $client = new GuzzleHttp\Client();
        $res = $client->request('GET', $url);

        if ($res->getStatusCode() == 200) {
            $body[] = json_decode($res->getBody());
            return OrderDAL::createOrdersFromJson($body)[0];
        } else {
            throw new ApiException("Error: Order not found", 400);
        }
    }

    private static function createOrdersFromJson(array $data) : array
    {
        $orders = array();
        foreach ($data as $jsonOrder) {
            $order = new Order(
                $jsonOrder->id,
                $jsonOrder->createdAt
            );
            $orders[] = $order;
        }
        return $orders;
    }
}

?>
<?php

abstract class UserDAL
{
  private static array $_users;

  public static function getById(int $id): User
  {
    self::readUsers();
    
    foreach(self::$_users as $key => $user) {
      if ($user["id"] == $id) {
        return new User(
          $user["id"],
          $user["username"],
          $user["password"],
          $user["mail"],
          $user["validationKey"]
        );
      }
    }

    return null;
  }

  public static function getByUsername(string $name): User|null
  {
    self::readUsers();

    foreach(self::$_users as $key => $user) {
      if ($user["username"] == $name) {
        return new User(
          $user["id"],
          $user["username"],
          $user["password"],
          $user["mail"],
          $user["validationKey"]
        );
      }
    }

    return null; 
  }

  public static function addUser(
    string $username,
    string $password,
    string $mail,
    string $validationKey
  ): void
  {
    self::readUsers();

    $user = new User(
      self::nextId(),
      $username,
      password_hash($password, PASSWORD_DEFAULT),
      $mail,
      $validationKey
    );

    self::$_users[] = array(
      "id" => $user->id(),
      "username" => $user->username(),
      "password" => $user->password(),
      "mail" => $user->mail(),
      "validationKey" => $user->validationKey(),
    );

    self::writeUsers();
  }

  public static function updateUser(User $newUser): void
  {
    self::readUsers();

    foreach(self::$_users as $key => $user) {
      if ($user["id"] == $newUser->id()) {
        self::$_users[$key] = array(
          "id" => $newUser->id(),
          "username" => $newUser->username(),
          "password" => $newUser->password(),
          "mail" => $newUser->mail(),
          "validationKey" => $newUser->validationKey(),
        );
      }
    }
    
    self::writeUsers();
  }

  private static function readUsers(): void
  {
    $path = __DIR__.'/../../static/users.json';
    $jsonString = file_get_contents($path);
    $jsonData = json_decode($jsonString, true);
    self::$_users = $jsonData;
  }

  private static function writeUsers(): void
  {
    $path = __DIR__.'/../../static/users.json';
    $jsonString = json_encode(self::$_users, JSON_PRETTY_PRINT);
    $fp = fopen($path, 'w');
    fwrite($fp, $jsonString);
    fclose($fp);
  }

  private static function nextId(): int
  {
    self::readUsers();
    $id = self::$_users[count(self::$_users) -1]["id"] + 1;
    return $id;
  }
}
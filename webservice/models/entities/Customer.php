<?php

class Customer {
    private string $_id;
    private string $_name;
    private string $_firstName;
    private string $_lastName;
    private string $_userName;
    private string $_profileFirstName;
    private string $_profileLastName;
    private string $_postalCode;
    private string $_city;
    private string $_companyName;
    private string $_createdAt;

    public function __construct(int $id, string $name, string $firstName, string  $lastName, string $userName, string $profileFirstName, 
        string $profileLastName, string $postalCode, string $city, string $companyName, string $createdAt)
    {
        $this->_id = $id;
        $this->_name = $name;
        $this->_firstName = $firstName;
        $this->_lastName = $lastName;
        $this->_userName = $userName;
        $this->_profileFirstName = $profileFirstName;
        $this->_profileLastName = $profileLastName;
        $this->_postalCode = $postalCode;
        $this->_city = $city;
        $this->_companyName = $companyName;
        $this->_createdAt = $createdAt;
    }

    public function toArray(): array
    {
        return array(
            "id" => $this->_id,
            "name" => $this->_name,
            "firstName" => $this->_firstName,
            "lastName" => $this->_lastName,
            "userName" => $this->_userName,
            "profileFirstName" => $this->_profileFirstName,
            "profileLastName" => $this->_profileLastName,
            "postalCode" => $this->_postalCode,
            "city" => $this->_city,
            "companyName" => $this->_companyName,
            "createdAt" => $this->_createdAt
        );
    }
}

?>
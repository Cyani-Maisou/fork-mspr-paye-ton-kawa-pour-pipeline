<?php

class Product {
    private string $_id;
    private string $_name;
    private int $_stock;
    private string $_price;
    private string $_description;
    private string $_color;
    private string $_createdAt;

    public function __construct(int $id, string $name, int  $stock, string  $price, string $description, string $color, string $createdAt)
    {
        $this->_id = $id;
        $this->_name = $name;
        $this->_stock = $stock;
        $this->_price = $price;
        $this->_description = $description;
        $this->_color = $color;
        $this->_createdAt = $createdAt;
    }

    public function toArray(): array
    {
        return array(
            "id" => $this->_id,
            "name" => $this->_name,
            "stock" => $this->_stock,
            "price" => $this->_price,
            "description" => $this->_description,
            "color" => $this->_color,
            "createdAt" => $this->_createdAt,
        );
    }
}

?>
<?php

class Order {
    private string $_id;
    private string $_createdAt;

    public function __construct(int $id, string $createdAt)
    {
        $this->_id = $id;
        $this->_createdAt = $createdAt;
    }

    public function toArray(): array
    {
        return array(
            "id" => $this->_id,
            "createdAt" => $this->_createdAt,
        );
    }
}

?>
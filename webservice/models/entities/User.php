<?php

class User
{
  private int $_id;
  private string $_username;
  private string $_password;
  private string $_mail;
  private string|null $_validationKey;

  public function __construct(int $id, string $username, string  $password, string  $mail, string $validationKey = null)
  {
    $this->_id = $id;
    $this->_username = $username;
    $this->_password = $password;
    $this->_mail = $mail;
    $this->_validationKey = $validationKey;
  }

  public function setUsername(string $username): void
  {
    $this->_username = $username;
  }

  public function setMail(string  $mail): void
  {
    $this->_mail = $mail;
  }

  public function setValidationKey(string|null $validationKey): void
  {
    $this->_validationKey = $validationKey;
  }

  public function id(): int
  {
    return $this->_id;
  }

  public function username(): string
  {
    return $this->_username;
  }

  public function password(): string
  {
    return $this->_password;
  }

  public function mail(): string
  {
    return $this->_mail;
  }

  public function validationKey(): string|null
  {
    return $this->_validationKey;
  }
}
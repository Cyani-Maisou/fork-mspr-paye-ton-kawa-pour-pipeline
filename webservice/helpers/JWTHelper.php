<?php

abstract class JWTHelper
{
  public static function generate(array $payload): string
  {
    $secret = getenv('JWT_SECRET');

    $header = json_encode([
      'typ' => 'JWT',
      'alg' => 'HS256'
    ]);

    $jsonPayload = json_encode($payload);

    $base64UrlHeader = self::base64UrlEncode($header);
    $base64UrlPayload = self::base64UrlEncode($jsonPayload);

    $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);

    $base64UrlSignature = self::base64UrlEncode($signature);

    $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

    return $jwt;
  }

  public static function validate(string $jwt): bool
  {
    $secret = getenv('JWT_SECRET');

    $tokenParts = explode('.', $jwt);

    if (count($tokenParts) != 3) {
      return false;
    }

    $header = base64_decode($tokenParts[0]);
    $payload = base64_decode($tokenParts[1]);
    $signatureProvided = $tokenParts[2];

    // faudra checker l'expiration dans la payload

    // on reconstruit une signature pour voir si ça correspond
    $base64UrlHeader = self::base64UrlEncode($header);
    $base64UrlPayload = self::base64UrlEncode($payload);

    $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
    $base64UrlSignature = self::base64UrlEncode($signature);

    return ($base64UrlSignature === $signatureProvided);
  }

  private static function base64UrlEncode($text)
  {
    return str_replace(
      ['+', '/', '='],
      ['-', '_', ''],
      base64_encode($text)
    );
  }
}
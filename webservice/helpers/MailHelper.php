<?php

use chillerlan\QRCode\QRCode;

abstract class MailHelper
{
  public static function sendValidationKey(string $to, string $qrCode): bool
  {
    $subject = 'PayeTonKawa: Validation de votre inscription';

    $headers = "MIME-Version: 1.0" . "\r\n"; 
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From:registration@payetonkawa.com';

    $htmlContent = ' 
    <html> 
      <head> 
          <title>Test</title> 
      </head> 
      <body> 
          <h1>Test HTML</h1> 
          <img src="' . $qrCode . '" alt="QR Code" width="400" height="400">
      </body> 
    </html>';

    return mail($to, $subject, $htmlContent, $headers);
  }
}

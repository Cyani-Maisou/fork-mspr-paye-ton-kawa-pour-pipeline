<?php

use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;

abstract class QRHelper
{
  public static function generateQR($data): string
  {
    $jsonData = json_encode($data);

    $options = new QROptions(
      [
        'eccLevel' => QRCode::ECC_L,
        'outputType' => QRCode::OUTPUT_MARKUP_SVG,
        'version' => 5,
      ]
    );

    $qrcode = (new QRCode($options))->render($jsonData);

    return $qrcode;
  }
}
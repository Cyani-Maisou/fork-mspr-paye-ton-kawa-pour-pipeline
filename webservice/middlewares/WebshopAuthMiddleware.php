<?php

class WebshopAuthMiddleware implements IMiddleware
{
  public function middlewareAction(IRequest $request): void
  {
    if (!$this->hasKey($request)) {
      $message = 'Error: please provide an authentication key';
      throw new InvalidAuthException($message, 400);
    }
    
    if (!$this->checkAuth($request)) {
      $message = 'Error: invalid authentication key';
      throw new InvalidAuthException($message, 400);
    }
  }

  private function checkAuth(IRequest $request): bool
  {
    $headerAuth = $request->headers()["authorization"];
    $headerParts = explode(' ', $headerAuth);

    if (!empty($headerParts[1])) {
      $key = $headerParts[1];

      return $key == getenv('WEBSHOP_SECRET');
    } else {
      return false;
    }
  }

  private function hasKey(IRequest $request): bool
  {
    return key_exists('authorization', $request->headers());
  }
}
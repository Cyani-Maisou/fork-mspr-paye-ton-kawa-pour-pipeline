<?php

class AuthMiddleware implements IMiddleware
{
  public function middlewareAction(IRequest $request): void
  {
    if (!$this->hasToken($request)) {
      $message = 'Error: please provide a token';
      throw new InvalidAuthException($message, 400);
    }
    
    if (!$this->checkAuth($request)) {
      $message = 'Error: invalid token';
      throw new InvalidAuthException($message, 400);
    }
  }

  private function checkAuth(IRequest $request): bool
  {
    $headerAuth = $request->headers()["authorization"];
    $headerParts = explode(' ', $headerAuth);

    if (!empty($headerParts[1])) {
      $jwt = $headerParts[1];

      return JWTHelper::validate($jwt);
    } else {
      return false;
    }
  }

  private function hasToken(IRequest $request): bool
  {
    return key_exists('authorization', $request->headers());
  }
}
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:paye_ton_kawa/userInterface/authentification/Auth.dart';

void main(){
  testWidgets('[authentification/Auth.dart] le widget affiche au moins un widget Form', (tester) async {
    await tester.pumpWidget(MaterialApp(home: Auth()));
    await tester.pumpAndSettle(); //nécessaire pour un statefulwidget
    final formFinder = find.byType(Form);
    expect(formFinder, findsOneWidget);
  });

  testWidgets('[authentification/Auth.dart] Le widget contient 2 champs texte', (tester) async {
    await tester.pumpWidget(MaterialApp(home: Auth()));
    await tester.pumpAndSettle();
    //TextFormField wrap un composant TextField, autant tester le moins spécialisé
    final fieldFinder = find.byType(TextField);
    expect(fieldFinder, findsNWidgets(2));
  });

  testWidgets('[authentification/Auth.dart] On trouve au moins un champ dont le texte est caché', (tester) async {
    await tester.pumpWidget(MaterialApp(home: Auth()));
    await tester.pumpAndSettle();

    final finder = find.byWidgetPredicate((widget) => widget is TextField && widget.obscureText == true);
    expect(finder, findsWidgets);
  });
}

// https://dhruvnakum.xyz/testing-in-flutter-widget-test
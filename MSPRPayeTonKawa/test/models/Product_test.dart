import 'package:flutter_test/flutter_test.dart';
import 'package:paye_ton_kawa/models/Product.dart';

void main(){
  test('Le constructeur de la classe Product initialise bien ses attributs', () {
    final product = Product('Produit-test', 0, '0', 'Description-test', 'Red', 0.00);
    expect(product, isNotNull);
    expect(product.name, 'Produit-test');
    expect(product.stock, 0);
    expect(product.id, '0');
    expect(product.description, 'Description-test');
    expect(product.couleur, 'Red');
    expect(product.prix, 0.00);
  });
}
# paye_ton_kawa

A new Flutter project.

En cas de soucis pour lancer le projet:
- Vérifier que le plugin Flutter est bien ajouté à Android Studio ou au projet
- Dans un terminal: `flutter pub get` et `flutter pub upgrade`
- Il peut parfois être nécessaire d'activer le plugin Dart dans Android Studio ou le projet

En cas d'autres problèmes:
- Vérifier que les urls utilisées dans les services sont correctes

## Docs:
Flutter: https://docs.flutter.dev/cookbook
Plugin RA: https://pub.dev/packages/ar_flutter_plugin


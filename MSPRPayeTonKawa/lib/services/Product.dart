import 'dart:io';
import 'dart:convert';

import '../models/Product.dart';

class ProductService {
  final String _baseApiUrl = 'http://192.168.159.190/webservice/revendeurs'; // penser à update l'adresse IP !
  final String _jwt;

  ProductService(this._jwt);


  // pour ne pas galérer avec la syntaxe des headers à chaque appel API
  Future<String> fetchWithToken(String url) async {
    // dart met des minuscules partout si on utilise juste http.get() c'est trop bien <3
    final client = HttpClient();
    final HttpClientRequest req = await client.getUrl(Uri.parse(url));
    req.headers.set('Authorization', "Bearer $_jwt", preserveHeaderCase: true);
    final response = await req.close();
    // il y a probablement d'autres types disponibles mais string reste le plus simple à implémenter et utiliser
    final stringData = await response.transform(utf8.decoder).join();
    client.close();
    return stringData;
  }

  Future<List<Product>> getList() async {
    final String url = '$_baseApiUrl/products';
    final String stringData = await fetchWithToken(url);
    final List<dynamic> rawList = jsonDecode(stringData);
    List<Product> formatedList = <Product>[];
    for(var obj in rawList){
      Product p = Product(
        obj['name'],
        obj['stock'] as int, // int.parse provoque une erreur sans aucune raison
        obj['id'],
        obj['description'],
        obj['color'],
        double.parse(obj['price']), 
      );
      formatedList.add(p);
    }
    return formatedList;
  }
}
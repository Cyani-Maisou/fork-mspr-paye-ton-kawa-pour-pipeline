import 'dart:convert';
import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

class UserService {
  static String? _jwt;
  final String _baseApiUrl = 'http://192.168.159.190/webservice/revendeurs'; // penser à update l'adresse IP !

  bool hasJwt(){
    if(_jwt != null && _jwt!.isNotEmpty) {
      return true;
    } else {
      return false;
    }
  }

  String? getJwt(){
    return _jwt;
  }

  Future<bool> register(String username, String password, String email) async{
    final String url = '$_baseApiUrl/register';
    final body = jsonEncode({
      'username': username,
      'password': password,
      'mail': email,
    });

    try {
      final http.Response response = await http.post(Uri.parse(url), body: body);
      if(response.statusCode == 200){
        // ATTENTION: il peut y avoir une réponse 200 alors que l'api plante, donc on évite certains cas d'erreur
        if(response.body.contains('<br')){
          return false;
        }
        return true;
      } else {
        // todo: gestion de l'exception en fonction du code d'erreur
        debugPrint("Problème dans UserService/register()");
        debugPrint(response.statusCode.toString());
        debugPrint(response.body);
        debugPrint(response.reasonPhrase);
        return false;
      }
    } catch (e) {
      debugPrint("Timeout de la requête http dans register(), veuillez vérifier l'url et la connexion");
      return false;
    }
  }

  Future<bool> login(String username, String password, Function? errorHandler) async {
    final String url = '$_baseApiUrl/login';
    final body = jsonEncode({
      'username': username,
      'password': password,
    });

    try {
      final http.Response response = await http.post(Uri.parse(url), body: body);
      if(response.statusCode == 200){
        // ATTENTION: il peut y avoir une réponse 200 alors que l'api plante, donc on évite certains cas d'erreur
        // (ex: l'api n'a pas les droits de lecture sur un fichier)
        if(response.body.contains('<br')){
          debugPrint("il y a eu un soucis côté API lors de la création de l'utilisateur");
          if(errorHandler != null) {
            errorHandler();
          }
          return false;
        }
        Map<String, dynamic> json = jsonDecode(response.body);
        debugPrint(json.toString());
        _jwt = json["access_token"];
        return true;
      } else {
        // todo: gestion de l'exception en fonction du code d'erreur
        debugPrint("Problème dans UserService/login()");
        debugPrint(response.statusCode.toString());
        debugPrint(response.body);
        debugPrint(response.reasonPhrase);
        return false;
      }
    } catch (e) {
      debugPrint("Erreur lors de la connexion");
      debugPrint(e.toString());
      return false;
    }
  }

  Future<bool> validateAccount(String username, String temporaryToken) async {
    final String url = '$_baseApiUrl/validate';
    final body = jsonEncode({
      'username': username,
      'validationKey': temporaryToken,
    });

    try {
      final http.Response response = await http.post(Uri.parse(url), body: body);
      if(response.statusCode == 200){
        // ATTENTION: il peut y avoir une réponse 200 alors que l'api plante, donc on évite certains cas d'erreur
        if(response.body.contains('<br')) {
          return false;
        }
        return true;
      } else {
        debugPrint("la validation ne marche pas");
        // todo: gestion de l'exception en fonction du code d'erreur
        debugPrint("Problème dans UserService/validateAccount()");
        debugPrint(response.statusCode.toString());
        debugPrint(response.body);
        debugPrint(response.reasonPhrase);
        return false;
      }
    } catch (e) {
      debugPrint("Erreur dans validateAccount(), veuillez vérifier l'url et la connexion");
      debugPrint(e.toString());
      return false;
    }
  }
}
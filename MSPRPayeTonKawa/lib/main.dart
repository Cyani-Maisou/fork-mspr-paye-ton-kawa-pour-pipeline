import 'package:flutter/material.dart';
import 'userInterface/authentification/Auth.dart';

void main() {
  runApp(const Home());
}

class Home extends StatelessWidget {
  const Home({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Paye ton kawa',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: Auth(),
    );
  }
}
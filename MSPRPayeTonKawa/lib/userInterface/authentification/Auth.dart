import 'package:flutter/material.dart';
import 'package:paye_ton_kawa/userInterface/authentification/NewAccount.dart';
import 'package:paye_ton_kawa/userInterface/product/ListProduct.dart';
import 'package:paye_ton_kawa/services/User.dart';

import '../ErrorPage.dart';

class Auth extends StatefulWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final UserService _userService = UserService();
  bool _isLoading = false;
  String _loginError = '';

  Auth({Key? key}) : super(key: key);

  @override
  State<Auth> createState() => _AuthState();
}

class _AuthState extends State<Auth> {

  String? simpleFieldValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ce champ est obligatoire';
    }
    return null;
  }

  void loginErrorCallback() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const ErrorPage(),
      ),
    );
  }

  void handleLoginButtonPressed(context){
    if(widget._formKey.currentState!.validate()){
      setState(() {
        widget._isLoading = true;
      });
           
      String username = widget._nameController.text;
      String password = widget._passwordController.text;
      widget._userService.login(username, password, loginErrorCallback).then((value) {
        if(value == true){
          redirectToList(context);
        }
        setState(() {
          widget._isLoading = false;
          widget._loginError = 'Mauvais identifiant ou mot de passe'; // todo: gérer les autres erreurs (connexion, etc)
        });
      });
    }
  }
  
  void redirectToList(context){
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
            ListProduct(title: 'Liste des produits'),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Authentification'),
      ),
      body: widget._isLoading ? const CircularProgressIndicator() : buildLoginForm(context),      
    );
  }

  Widget buildLoginForm(BuildContext context){
    return SingleChildScrollView(child: 
      Form(
        key: widget._formKey,
        child: Column(
          children: [
            Row(
              children: [
                SizedBox(
                  width: 200,
                  height: 300,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      validator: simpleFieldValidator,
                      controller: widget._nameController,
                      decoration: const InputDecoration(
                        hintText: 'Identifiant',
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 200,
                  height: 300,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      validator: simpleFieldValidator,
                      controller: widget._passwordController,
                      obscureText: true,
                      decoration: const InputDecoration(
                        hintText: 'Mot de passe',
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Center( //todo: afficher ça plus joliment
              child: Text(widget._loginError),
            ),
            Center(
              child: ElevatedButton(
                onPressed: () {
                  handleLoginButtonPressed(context);
                },
                child: const Text('Se connecter'),
              ),
            ),
            Row(
              children: [
                const Text('Pas de compte ?'),
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const NewAccount(),
                      ),
                    );
                  },
                  child: const Text('S\'inscrire '),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';
import 'package:paye_ton_kawa/services/User.dart';
import 'package:paye_ton_kawa/userInterface/authentification/Auth.dart';

import '../ErrorPage.dart';
import '../QRCodeReader.dart';


//envoi des données du nouveau compte et affichage d'un widget de chargement
class AccountConfirmation extends StatefulWidget {
  final String _username;
  const AccountConfirmation(this._username, {Key? key}) : super(key: key);

  @override
  State<AccountConfirmation> createState() => _AccountConfirmationState();
}

class _AccountConfirmationState extends State<AccountConfirmation> {
  bool loading = false;
  final _userService = UserService();

  @override
  void initState(){
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Validation de votre compte'),
      ),
      body: QRCodeReader(barcodeDetectionHandler),
    );
  }

  Future<void> barcodeDetectionHandler(Barcode barcode) async {
    if(barcode.type == BarcodeType.text 
      && barcode.rawValue != null 
      && barcode.rawValue!.isNotEmpty
      && !loading
    ){
      loading = true;
      final json = jsonDecode(barcode.rawValue!);
      final key = json['validationKey'];
      bool validated = await _userService.validateAccount(widget._username, key);
      // todo: améliorer la gestion d'erreur
      // actuellement l'application ouvre la page d'erreur si un qrcode contient validationKey avec la mauvaise clé
      if(!mounted) {
        return;
      }
      if(validated){
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Auth(),
          ),
        );
        return;
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const ErrorPage(),
          ),
        );
        return;
      }
    }
  }
}

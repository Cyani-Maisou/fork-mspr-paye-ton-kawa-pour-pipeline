import 'package:flutter/material.dart';
import 'package:paye_ton_kawa/userInterface/authentification/AccountConfirmation.dart';
import 'package:paye_ton_kawa/services/User.dart';

import '../ErrorPage.dart';

class NewAccount extends StatefulWidget {
  final double fieldsHeight = 100;
  final double fieldsWidth = 400;

  const NewAccount({Key? key}) : super(key: key);

  @override
  State<NewAccount> createState() => _NewAccountState();
}

class _NewAccountState extends State<NewAccount> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final UserService _userService = UserService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Créer un compte'),
      ),
      // Si le formulaire est trop grand, on pourra scroll
      body: SingleChildScrollView(
        child: buildForm(),
      ),
    );
  }

  String? simpleFieldValidator(String? value) {
    if (value == null || value.isEmpty) {
      return 'Ce champ est obligatoire';
    }
    return null;
  }

  Future<void> sendForm(
    TextEditingController nameController,
    TextEditingController passwordController,
    TextEditingController emailController
  ) async {
    bool registered = await _userService.register(nameController.text, passwordController.text, emailController.text);
    if(!mounted) {
      return;
    }
    if(registered){
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AccountConfirmation(nameController.text),
        ),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const ErrorPage(),
        ),
      );
    }    
  }

  String? confirmationFieldValidator(
      TextEditingController toCompare, String? value) {
    if (value == null || value.isEmpty) {
      return 'Ce champ est obligatoire';
    } else if (value != toCompare.text) {
      return 'Les valeurs sont différentes';
    }
    return null;
  }

  Widget buildForm() {
    final TextEditingController nameController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();
    final TextEditingController emailController = TextEditingController();

    return Form(
      key: _formKey,
      child: Column(
        children: [
          SizedBox(
            width: widget.fieldsWidth,
            height: widget.fieldsHeight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                validator: simpleFieldValidator,
                controller: nameController,
                decoration: const InputDecoration(
                  labelText: 'Identifiant',
                  hintText: 'Billy',
                ),
              ),
            ),
          ),
          SizedBox(
            width: widget.fieldsWidth,
            height: widget.fieldsHeight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                validator: simpleFieldValidator,
                controller: passwordController,
                obscureText: true,
                decoration: const InputDecoration(
                  labelText: 'Mot de passe',
                  hintText: 'motdep4sse!',
                ),
              ),
            ),
          ),
          SizedBox(
            width: widget.fieldsWidth,
            height: widget.fieldsHeight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                validator: (value) =>
                    confirmationFieldValidator(passwordController, value),
                obscureText: true,
                decoration: const InputDecoration(
                  labelText: 'Confirmer le mot de passe',
                  hintText: 'motdep4sse!',
                ),
              ),
            ),
          ),
          SizedBox(
            width: widget.fieldsWidth,
            height: widget.fieldsHeight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                controller: emailController,
                validator: simpleFieldValidator,
                decoration: const InputDecoration(
                  labelText: 'Email',
                  hintText: 'exemple@mail.com',
                ),
              ),
            ),
          ),
          SizedBox(
            width: widget.fieldsWidth,
            height: widget.fieldsHeight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                validator: (value) => confirmationFieldValidator(emailController, value),
                decoration: const InputDecoration(
                  labelText: 'Confirmer l\'email',
                  hintText: 'exemple@mail.com',
                ),
              ),
            ),
          ),
          Center(
            child: ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  sendForm(nameController, passwordController, emailController);
                }
              },
              child: const Text('S\'inscrire'),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

class QRCodeReader extends StatefulWidget { // pourrait probablement être stateless avec un petit refacto
  final ValueSetter<Barcode> _detectionCallback; //fonction qui saura quoi faire avec le QRCode -> single responsibility
  const QRCodeReader(this._detectionCallback, {Key? key}) : super(key: key);

  @override
  State<QRCodeReader> createState() => _QRCodeReaderState();
}

class _QRCodeReaderState extends State<QRCodeReader> {
  @override
  Widget build(BuildContext context) {
    return MobileScanner(
      controller: MobileScannerController(
        facing: CameraFacing.back,
        torchEnabled: false,
      ),
      onDetect: (capture) {
        
        final List<Barcode> barcodes = capture.barcodes;
        for (final barcode in barcodes) {
          debugPrint('Barcode found! ${barcode.rawValue}');
          widget._detectionCallback(barcode);
        }
      },
    );
  }
}

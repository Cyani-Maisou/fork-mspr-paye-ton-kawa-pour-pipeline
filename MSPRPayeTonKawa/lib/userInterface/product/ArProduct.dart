import 'package:ar_flutter_plugin/datatypes/config_planedetection.dart';
import 'package:ar_flutter_plugin/datatypes/node_types.dart';
import 'package:ar_flutter_plugin/managers/ar_anchor_manager.dart';
import 'package:ar_flutter_plugin/managers/ar_location_manager.dart';
import 'package:ar_flutter_plugin/managers/ar_object_manager.dart';
import 'package:ar_flutter_plugin/managers/ar_session_manager.dart';
import 'package:ar_flutter_plugin/models/ar_node.dart';
import 'package:ar_flutter_plugin/widgets/ar_view.dart';
import 'package:flutter/material.dart';
import 'package:vector_math/vector_math_64.dart';

class ArProduct extends StatefulWidget {
  const ArProduct({Key? key}) : super(key: key);

  @override
  State<ArProduct> createState() => _ArProductState();
}

class _ArProductState extends State<ArProduct> {
  ARSessionManager? arSessionManager;
  ARObjectManager? arObjectManager;
  ARNode? arProductNode;

  // pour éviter de bouffer toute la mémoire
  @override
  void dispose() {
    super.dispose();
    arSessionManager!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Aperçu du produit'),
      ),
      body: Container(
        child: Stack(
          children: [
            ARView(
              onARViewCreated: onARViewCreated,
              planeDetectionConfig: PlaneDetectionConfig.horizontalAndVertical,
            ),
          ],
        ),
      ),
    );
  }

  // https://pub.dev/documentation/ar_flutter_plugin/latest/managers_ar_session_manager/ARSessionManager-class.html
  void onARViewCreated(
      ARSessionManager arSessionManager,
      ARObjectManager arObjectManager,
      ARAnchorManager arAnchorManager,
      ARLocationManager arLocationManager){
    //important, ça permet de charger la fenêtre/caméra correctement
    this.arSessionManager = arSessionManager;
    this.arObjectManager = arObjectManager;

    // https://pub.dev/documentation/ar_flutter_plugin/latest/managers_ar_session_manager/ARSessionManager/onInitialize.html
    this.arSessionManager!.onInitialize(
      showFeaturePoints: false,
      showPlanes: true,
      customPlaneTexturePath: null,
      showWorldOrigin: false,
      handleTaps: false,
    );
    //this.arObjectManager!.onInitialize(); //je sais pas si on a besoin de cette ligne mais elle est dans tous les exemples

    loadProductNode();
  }

  Future<void> loadProductNode() async{
    var newNode = ARNode(
      // en théorie ou pourrait gérer les .glb aussi mais j'y arrive paaaaaas
        type: NodeType.localGLTF2,
        // Même s'il n'est pas précisé ici, le .bin est nécessaire
        uri: "assets/3d_models/Chicken_01.gltf",
        scale: Vector3(0.1, 0.1, 0.1),
        //position: axe rouge (droite), axe vert(haut), axe bleu(derrière) -> pour tester ça, mettre showWorldOrigin à true (ligne 64)
        position: Vector3(0, 0, -0.4)
      );
    await arObjectManager!.addNode(newNode);
  }
}

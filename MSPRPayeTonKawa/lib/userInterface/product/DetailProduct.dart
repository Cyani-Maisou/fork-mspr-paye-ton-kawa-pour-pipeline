import 'package:flutter/material.dart';
import 'package:paye_ton_kawa/models/Product.dart';

import 'ArProduct.dart';

class DetailProduct extends StatefulWidget {
  Product _product;

  DetailProduct(this._product, {Key? key}) : super(key: key);

  @override
  State<DetailProduct> createState() => _DetailProductState();
}

class _DetailProductState extends State<DetailProduct> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._product.name),
      ),
      body: ListView(
        padding: const EdgeInsets.only(top:4.0,bottom: 4.0,left:20,right: 20),
        children: [
          const SizedBox(height: 20),
          const Text('Description', style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: const EdgeInsets.only(top:8.0,bottom: 12.0),
            child: Text(widget._product.description),
          ),
          const SizedBox(height: 15),
          const Text('Couleur', style: TextStyle(fontWeight: FontWeight.bold),
          ),
          Padding(
            padding: const EdgeInsets.only(top:8.0,bottom: 12.0),
            child: Text(widget._product.couleur),
          ),
          const SizedBox(height: 15),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text('Prix : ', style: TextStyle(fontWeight: FontWeight.bold),),
              Text(widget._product.prix.toString(), ),
            ],
          ),
          const SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text('Stock : ' , style: TextStyle(fontWeight: FontWeight.bold),),
              Text(widget._product.stock.toString(),),
            ],
          ),
          const SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                child: const Text("Réalité Augmentée"),
                onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ArProduct(),
                    ),
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:paye_ton_kawa/userInterface/product/DetailProduct.dart';
import 'package:paye_ton_kawa/models/Product.dart';
import 'package:paye_ton_kawa/services/User.dart';

import '../../services/Product.dart';

class ListProduct extends StatefulWidget {
  String title;
  ListProduct({required this.title, Key? key}) : super(key: key);

  @override
  State<ListProduct> createState() => _ListProductState();
}

class _ListProductState extends State<ListProduct> {

  late Future<List<Product>> _product;
  final UserService _userService = UserService();
  ProductService? _productService;

  @override
  void initState(){
    super.initState();
    // si on est authentifié on initialise _productService
    if(_userService.hasJwt()){
      _productService = ProductService(_userService.getJwt()!);
      _product = _productService!.getList();
    }
    // todo: gestion d'erreur + fallback si on est déconnecté
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(widget.title),
      ),
      body: FutureBuilder<List<Product>>(
        future: _product,
        builder: (context, snapshot) {
          if(snapshot.hasData){
            return _buildListView(snapshot.data!);
          } else if (snapshot.hasError){
            return Center(
              child: Text(
                'Impossible de récupérer les données : ${snapshot.error}',
              ),
            );
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  _buildListView(List<Product> presentProduct) {
    return ListView.builder(
        itemCount: presentProduct.length,
        itemBuilder: (context, index) {
          return Column(
            children: [
              ListTile(
                leading: const Icon(Icons.adjust_outlined),
                title: _buildRow(context, presentProduct[index]),
              ),

            ],
          );
        }
    );
  }

  _buildRow(BuildContext context, Product product){
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(child: Text(product.name)),            
            ElevatedButton(
              child: Row(
                children: const [
                  Text("Voir le détail"),
                ],
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailProduct(product)
                  ),
                );
              },
            ),
          ],
        )
      ],
    );
  }
}